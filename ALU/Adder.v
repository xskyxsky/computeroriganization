module Adder(Cin, A, x, Add_Carry, Zero, Add_Overflow, Add_Sign, Add_Result);
	input[31: 0] A, x;
	input Cin;
	output reg[31: 0] Add_Result;
	output reg Add_Carry; 	// 进位标志
	
	output Add_Overflow, Add_Sign, Zero;
	
	always @(A or x or Cin)
		{Add_Carry, Add_Result} = A + x + Cin;

	//全0为1，把所有位或起来再取反
	assign Zero = ~|Add_Result;

	//符号位是最高位
	assign Add_Sign = Add_Result[31];	

	//溢出标志
	assign Add_Overflow = Add_Carry ^ Add_Sign ^ x[31] ^ A[31];
endmodule