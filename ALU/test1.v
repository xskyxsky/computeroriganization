module test1(A, B, L);
	input A, B;
	output L;
	
	// wire a = 1'b1;
	// or (L, a, a);
	
	wire a1, a2, Anot, Bnot;
	
	not (Anot, A);
	not (Bnot, B);		
	
	and G1(a1, A, B);
	and G2(a2, Anot, Bnot);

	or (L, a1, a2);
	
	// or (L, A, B);
	
endmodule