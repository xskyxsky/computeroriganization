// ALU Model
module ALU(A, B, ALUctr, zero, overflow, Result);
	input[31: 0] A, B; 	//  两个输入的32位操作数
	input[2: 0] ALUctr;	//  控制信号编码
	
	// 加法器
	wire Cin;	//进位
	//加法器的输出结果（是中间结果）
	wire Add_Carry, Add_Overflow, Add_Sign;
	wire[31: 0] Add_Result; 

	
	// 控制信号
	wire SUBctr, OVctr, SIGctr;	
	// SUBctr = 1做减法运算， 为0 做减法运算
	// OVCtr 控制是否需要溢出判断，OVCtr = 1时要进行溢出判断
	// 如果溢出，则overflow置为1；否则就算溢出overflow也不为1
	// SIGctr = 1执行带符号整数比较小于置1，=0无符号比较小于置为1

	wire[1: 0] OPctr;
	// OPctr = 1 控制选择三种运算作为运算结果（加减、按位或、小于置1），所以是两位
	
	//中间结果
	wire[31: 0] m, n, r, x, y;
	wire z, t;
	
	output zero, overflow;	//零标志，溢出标志
	output[31: 0] Result; 	//32位输出操作数

	// 0 和 1位扩展
	assign m = {32{1'b0}};
	assign n = {32{1'b1}}; 	

	// 中间结果赋值
	assign y = A | B;
	assign x = {32{SUBctr}} ^ B;
	assign z = Cin ^ Add_Carry;
	assign t = Add_Overflow ^ Add_Sign;
	assign Cin = SUBctr;

	// 根据表格5.3（137页）赋值
	assign SUBctr = ALU[2];
	assign OVctr = !ALUctr[1] & ALUctr[0];
	assign SIGctr = ALUctr[0];
	assign OPctr[1] = ALUctr[2] & ALUctr[1];
	assign OPctr[0] = !ALUctr[2] & ALUctr[1] & !ALUctr[0];

	//输出结果
	assign overflow = Add_Overflow & OVctr;

	// 加法器输入和输出
	Adder adder(Cin, A, x, Add_Carry, zero, Add_Overflow, Add_Sign, Add_result);

	// 二选一选择器输入输出
	MUX2to1 m1(z, t, SIGctr, Less);
	MUX2to1 m2(m, n, Less, r);

	//三选一选择器输入和输出
	MUX3to1 m3(Add_Result, y, r, OPctr, Result);
endmodule

	
	
	