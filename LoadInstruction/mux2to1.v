module mux2to1(input [31:0] x, 
			   input [31:0] y,
			   input ctr,
			   output reg [31:0] res
			   );

	always@(ctr)
	begin
		case(ctr)
			1'b0:res=x;
			1'b1:res=y;
		endcase
	end

endmodule