
module LoadInstruction(
  input clk, Branch, Jump, Zero, run,
  output [31:0] instruction
);

wire[4:0] addmem;
wire[29:0] Newpc, PC_1, imm30, PC_2, PC_3, PC_12;
wire Branch_zero;

reg[29:0] PC = 0;
reg reset = 1;

//assign addmem = PC << 2;  左移两位，相当于 {PC[29:0], 2'b00}
assign addmem = {PC[29:0], 2'b00}; 	// 地址;[

instrumem InsMem(addmem, instruction); 	// 取指令部件


always @(negedge clk)
   if (run == 1)
      begin
         if (reset == 1)
            begin
               PC <= 0;
               reset <= 0;
            end
         else
            begin
               PC <= Newpc;  
            end
      end
assign PC_1 = PC + 1;	//顺序执行
assign imm30 = {{14{instruction[15]}}, instruction[15:0]};	//SEXT(imm16)	
assign PC_2 = PC_1 + imm30;
assign PC_3 = {PC[29:26], instruction[25:0]};	//PC[31:28] + Target[25:0]
assign Branch_zero = Branch & Zero;
mux2to1 mux4(PC_1, PC_2, Branch_zero, PC_12);

mux2to1 mux5(PC_12, PC_3, Jump, Newpc);
endmodule




